import { useState } from 'react';

import { UserGroup } from '../UserUtils/UserGroup';

import { ReadJsonFile } from './InputJson';
export function ListingJson() {
  const [data, setData] = useState(null);
  let users = data;

  console.log(users);
  return (
    <div>
      <div className='p-3 text-center'>
        <ReadJsonFile elements={data} setData={setData} />
      </div>
      <div className='container'>
        <div className='row py-5'>
          {users
            ? users.map((info, i) => {
                return <UserGroup key={i} datos={info} groupNumber={i} />;
              })
            : 'No hay nada que mostrar aun, intenta cargar un archivo'}
        </div>
      </div>
    </div>
  );
}
