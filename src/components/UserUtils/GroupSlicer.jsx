export function Slicer({ array, size }) {
  let arregleCompleto = array;
  let arregloSeccionado = [];
  const TAMAÑO_GRUPOS = size;
  for (let i = 0; i < arregleCompleto.length; i += TAMAÑO_GRUPOS) {
    let pedazo = arregleCompleto.slice(i, i + TAMAÑO_GRUPOS);
    arregloSeccionado.push(pedazo);
  }
  return arregloSeccionado;
}
