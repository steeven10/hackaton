import { Randomizer } from '../UserUtils/Randomizer';
import { Slicer } from '../UserUtils/GroupSlicer';
import { useState } from 'react';

export const ReadJsonFile = ({ setData, elements }) => {
  //Estado de los inputs para el tamaño del gruo
  const [number, setNumber] = useState(1);
  //Estado para almacenar los datos del json cargado
  const [temp, setTemp] = useState(null);
  let jsonInfo = [];

  //Funcion que actializa los input numericos
  const actualizarNumero = (value) => {
    setNumber(value);
    console.log(typeof number);
  };

  //funcion que recibe el input file
  const onChange = (event) => {
    const fileReader = new FileReader();
    fileReader.readAsText(event.target.files[0], 'UTF-8');
    fileReader.onload = (event) => {
      let info = JSON.parse(event.target.result);
      for (let element in info) {
        jsonInfo.push(info[element]);
      }
      //se almacenan los datos del json en estado
      let test = jsonInfo[0];
      setTemp(test);
    };
  };

  //Funcion del boton generar los grupos
  const onGenerar = () => {
    //console.log(temp);
    let prev = temp;
    prev = Randomizer(prev);
    prev = Slicer({ array: prev, size: parseInt(number) });
    setData(prev);
  };

  return (
    <div className='container '>
      <div className=''>
        <br />
        <h4 className='mt-1'>1. Define el tamaño de los grupos</h4>
        <label htmlFor='tamanio'>Tamaño!</label>
        <br />
        <input
          type='range'
          value={number}
          step='1'
          max='30'
          className='form-range'
          onChange={(e) => {
            actualizarNumero(e.target.value);
          }}
          style={{ width: '350px' }}
          id='tamanio'></input>
        <br />
        <input
          className='m-2'
          readOnly
          type='number'
          name='input_size'
          id='input_size'
          value={number}
          onChange={(e) => {
            actualizarNumero(e.target.value);
          }}
        />
        <h4 className='mt-1'>2. Carga Aqui tu propio archivo JSON</h4>
        <input className='m-2' type='file' onChange={onChange} />
        <h4 className='mt-1'>3. Da clic en generar!</h4>
        <button onClick={onGenerar}>Generar!</button>
      </div>
    </div>
  );
};
