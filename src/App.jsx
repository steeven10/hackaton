import './App.css';

import { Link, Route, Routes } from 'react-router-dom';

import { ArchivoPropio } from './components/archivoPropio/archivoPropio';
import { Excel } from './pages/excel';
import { Footer } from './components/footer/Footer';
import { Home } from './components/Home/Home';
import { Json } from './pages/Json';
import { ListingJson } from './components/JsonListing/ListingJson';
import { Narbar } from './components/narbar/narvar';

function App() {
  return (
    <div className='App'>
      <Narbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/excel' element={<Excel />} />
        <Route path='/json' element={<Json />} />
        <Route path='/tuArchivo' element={<ArchivoPropio />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
