import default_foto from '../../assets/img/default.jpeg';
const default_data = {
  Nombres: 'Sin nombre',
  Cargo: 'Sin cargo',
  Foto: default_foto,
};

export function UserCard({ Nombres, Cargo, Foto }) {
  Nombres ? Nombres : default_data.Nombres;
  Foto ? Foto : default_foto;
  Cargo ? Cargo : default_data.Cargo;
  return (
    <div className='col-sm-6 col-xl-4 m-auto'>
      <div
        className='card m-3'
        style={{ maxWidth: '240px', maxHeight: '300px' }}>
        <div className='card-body'>
          <h5 className='card-title text-center p-1'>{Nombres}</h5>
          <img
            className=''
            src={Foto}
            style={{
              display: 'block',
              margin: 'auto',
              maxWidth: '100px',
              maxHeight: '100px',
            }}
            alt={Nombres}
            onError={(e) => {
              console.log(e.target.src);
              e.target.src = default_data.Foto;
            }}
          />
          <h6 className={'text-center p-1'}>{Cargo}</h6>
        </div>

        {}
      </div>
    </div>
  );
}
