import { FormExcel } from "../components/excellist/inputExcel"
import { useState,} from "react"
export function Excel() {
    const [group,setGroup]=useState(null)
    const actualizarGrupo = (value) => {
        setGroup(value)
    }
  
  console.log((group))
    return (
        <div>
            <div><h1 className="text center">pagina de excel</h1></div>
            <div className="input-group mb-3 row">
                <div className="col-8"></div>
                <div className="col-4">
                    <input type="number" 
                    aria-label="hola"
                     name='input_size'
                     id='input_size'
                     value={group}
                     onChange={(e)=>{
                      actualizarGrupo(e.target.value)   
                     }}
                     />
                
                </div>

            </div>
            <div className="container"><FormExcel  val={group}/></div>
        </div>
    )
}