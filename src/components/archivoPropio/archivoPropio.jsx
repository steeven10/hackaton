import { useState } from 'react';
export function ArchivoPropio() {
  const [tamanio, setTamanio] = useState(0);
  const [textjson, settextjson] = useState('');

  const inputTamanio = ({ target }) => {
    setTamanio(target.value);
  };

  const inputjson = ({ target }) => {
    settextjson(target.value);
  };

  const mostrar = (e) => {
    e.preventDefault(tamanio, textjson);
    console.log(parseInt(tamanio));
    console.log(JSON.parse(textjson));
  };
  return (
    <div className='container'>
      <div className='row'>
        <div className='col-3'></div>
        <div className='col-6'>
          <h1 className='text-center p-5'>
            Escribe tu propio archivo en formato Json
          </h1>

          <form onSubmit={mostrar}>
            <div className='mb-3'>
              <label htmlFor='tamanio' className='form-label'>
                Tamaño de los grupos
              </label>
              <input
                name='Tamanio'
                type='number'
                className='form-control'
                id='tamanio'
                value={tamanio}
                onChange={inputTamanio}
              />
            </div>
            <div className='mb-3'>
              <label htmlFor='textjson' className='form-label'>
                JSON
              </label>
              <textarea
                name='textjson'
                type='text'
                className='form-control'
                id='textjson'
                value={textjson}
                onChange={inputjson}
              />
            </div>

            <button type='submit' className='btn btn-primary'>
              Generar grupos
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
