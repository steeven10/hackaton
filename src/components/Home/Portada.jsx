import ImgPortada from '../../assets/img/imagen-hackatón.jpg';
export function Portada() {
  return (
    <div className='card mb-3'>
      <img
        src={ImgPortada}
        className='card-img-top'
        style={{ width: '1534px', height: '610px' }}
        alt='...'
      />
      <div className='card-body'></div>
    </div>
  );
}
