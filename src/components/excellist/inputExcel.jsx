import * as XLSX from 'xlsx';
import { useState } from 'react';
import { UserCard } from '../UserUtils/UserCard';
import { Randomizer } from '../UserUtils/Randomizer';
import { Slicer } from '../UserUtils/GroupSlicer';
import { UserGroup } from '../UserUtils/UserGroup';
export function FormExcel({val,onSubmit}) {
  const [datos, setDatos] = useState([]);
  const [data,setData] = useState([]);
  console.log(parseInt(val))
  const readExcel = (file) => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsArrayBuffer(file);
      fileReader.onload = (e) => {
        const bufferArray = e.target.result;
        const wb = XLSX.read(bufferArray, { type: 'buffer' });
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        const data = XLSX.utils.sheet_to_json(ws);
        resolve(data);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
    
      promise.then((d) => {
         {/* let out = Randomizer(d);
          out = Slicer({ array: out, size: parseInt(val) });
          setDatos(out);
          console.log(out);

      */}
      setDatos(d)
      });
    

  };
  const onGenerar = () => {
    let out = Randomizer(datos)
    out = Slicer({ array: out, size: parseInt(val) })
    setData(out)
  }
  return (
    <div>
      <input
        required
        type='file'
        name='file'
        id='file'
        onChange={(e) => {
          const file = e.target.files[0];
          readExcel(file);
        }}
        placeholder='archivo de excel'
      />
        <button className="btn btn-success" 
                onClick={onGenerar} 
                > generar grupos </button>  
      <div className='container'>
        <div className='row py-5'>
          {data
            ? data.map((info, i) => {
                return <UserGroup key={i} datos={info} groupNumber={i} />;
              })
            : 'No hay nada que mostrar'}
        </div>
      </div>
    </div>
  );
}
