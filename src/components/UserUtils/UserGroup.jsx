import { UserCard } from './UserCard';

export function UserGroup({ groupNumber, datos }) {
  //let num = props.groupNumber;
  //let info = props.datos;
  // console.log(info);
  return (
    <div className='row container text-center m-4 border border-dark'>
      <h2>Grupo # {groupNumber + 1} </h2>
      {datos.map((user) => {
        return (
          <UserCard
            key={user.Nombres}
            Nombres={user.Nombres}
            Foto={user.Foto}
            Cargo={user.Cargo}
          />
        );
      })}
    </div>
  );
}
